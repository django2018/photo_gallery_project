rem debug batch: file.bat > file.log > 2>&1
rem run activate of virtualenv, open cmd, after activate, them call file run.bat
@echo off
rem path, pythonpath: biến môi trường cho python
set path=%path%;C:\virtualenv\python2.7\Scripts
set pythonpath=%pythonpath%;C:\virtualenv\python2.7\Lib\site-packages
rem echo %path%&echo.
rem echo %pythonpath%
cls
if defined activate echo %activate%
echo Choose number to act
echo ------*------
echo 1.runserver
echo 2.migrate
echo 3.makemigrations
echo 4.createsuperuser
echo 5.activate
echo 6.deactivate
set /p index=
if %index% == 1 set start=python manage.py runserver
if %index% == 2 set start=python manage.py migrate
if %index% == 3 set start=python manage.py makemigrations
if %index% == 4 set start=python manage.py createsuperuser
if %index% == 5 set start="cmd /k activate & set activate=activated & run.bat"
if %index% == 6 set start="cmd /k deactivate & set activate=deactivated & run.bat"
echo on
%start%
@echo off
pause
call run.bat
exit

rem check python version
rem >>> import sys
rem >>> sys.version or >>> sys.version_info or >>> sys.hexversion

rem check django version
rem >>> import django
rem >>> django.VERSION
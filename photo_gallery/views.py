from django.shortcuts import render

from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from photo_gallery.models import Item, Photo

class ItemList(TemplateView):

    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(ItemList, self).get_context_data(**kwargs)
        context['item_list'] = Item.objects.all()
        return context

class ItemListView(ListView):

    model = Item
    
class ItemDetailView(DetailView):

    model = Item
    
class PhotoDetailView(DetailView):

    model = Photo
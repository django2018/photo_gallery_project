from django.conf.urls import url
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve

from photo_gallery.models import Item, Photo
from photo_gallery.views import ItemList, ItemListView, ItemDetailView, PhotoDetailView

urlpatterns = [
    url(r'^$', 
        ItemList.as_view(template_name='index.html'),
        name='index',
    ),
    url(r'^items/$', ItemListView.as_view(),
        name='item_list'
    ),
    url(r'^items/(?P<pk>\d+)/$', ItemDetailView.as_view(),
        name='item_detail'
    ),
    url(r'^photos/(?P<pk>\d+)/$', PhotoDetailView.as_view(),
        name='photo_detail'
    ),
    url(r'^media/(?P<path>.*)$', serve,{'document_root':settings.MEDIA_ROOT}),
]